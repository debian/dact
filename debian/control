Source: dact
Section: utils
Priority: optional
Maintainer: Debian QA Group <packages@qa.debian.org>
Build-Depends: debhelper-compat (= 13), zlib1g-dev, libbz2-dev, libmcrypt-dev, liblzo2-dev
Rules-Requires-Root: no
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/debian/dact
Vcs-Git: https://salsa.debian.org/debian/dact.git
Homepage: https://www.rkeene.org/oss/dact

Package: dact
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: multi-algorithm compression
 DACT compresses each block within the file with all its known algorithms and
 uses the block with the best compression ratio.
 .
 DACT can encrypt the compressed data with one of two algorithms.
 .
 Compression time for DACT is slow as each block is compressed multiple times.
 .
 Current supported compression algorithms include RLE, Delta, Text, Zlib,
 Modified Zlib, Bzip2 and Seminibble Encoding.
